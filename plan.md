The PCWCamera system consists of a central Controller and 3 subsystems running
concurrently which it controls.

## Central Controller

The system in charge of coordinating the rest and maintaining internal state.
Received commands are passed on to the Recorder. Once the Uploader has a
location, this location is recorded in a table (backed to disk) mapping IDs to
locations.

When the system starts up, it first checks for any temporary data left over from
an unclean shutdown and removes it. It loads up the upload queue and upload
location table from disk and kicks off the subsystems.

## Subsystem 1: Http Server

Server serves a simple page *(mostly unimplimented)* that shows current status
(disk usage, queue lengths, recording status), list of upload links sorted by job
id or timestamp, and a preview of camera setup. *Does this page need controls for
adjusting camera settings?*

POST requests are expected in the following form:

key | value | description
--- | --- | ---
start | ID | begin recording a video with given ID
pause | *none* | pause recording 
resume | *none* | resume recording
stop | *none* | stop recording
upload |*none* | should be paired with stop, if present also upload the video after stopping the recording

Examples of POST requests:  
start recording video ID 12345: `curl -X POST ${address}:5000 -d "start=12345"`  
pause recording video: `curl -X POST ${address}:5000 -d "pause"`  
resume recording video: `curl -X POST ${address}:5000 -d "resume"`  
stop and upload recording: `curl -X POST ${address}:5000 -d "stop" -d "upload"`  
just stop recording: `curl -X POST ${address}:5000 -d "stop"`

## Subsystem 2: Recorder

The Recorder subsystem is responsible for handling the camera. There are 3
states this system can be in:
 - **Idle**
 - **Recording**
 - **Paused**

| | **begin recording** | **pause recording** | **resume recording** | **stop recording**
---: | :---: | :---: | :---: | :---:
**Idle** | Recording | - | - | -
**Recording** | - | Paused | - | Idle
**Paused** | - | - | Recording | Idle

Once the Recorder has received a Stop and Upload command, it saves the video
and passes the filepath to the Uploader.

## Subsystem 3: Uploader

The uploader subsystem is responsible for uploading recorded videos (youtube?
ftp server?). It maintains a queue *(TODO: backed to disk)* of videos to upload and
uploads them. Once a video is uploaded, its location is handed back to the
Controller.
