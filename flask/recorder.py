#!/usr/bin/env python

from enum import Enum
from picamera import PiCamera

class RecState(Enum):
    IDLE = 1
    RECORDING = 2
    PAUSED = 3

class Recorder():
    """handle camera state"""
    def __init__(self):
        self.__state = RecState.IDLE
        self.__recording = None
        self.camera = PiCamera(resolution = (1280, 720), framerate = 30)

    def start(self, ID):
        """start a recording with filename matching ID if camera is idle,
           otherwise continue whatever camera was doing"""
        if self.__state == RecState.IDLE:
            filename = 'data/%s.h264' % ID
            vidfile = open(filename, 'w+b')
            self.camera.start_recording(vidfile)
            self.__recording = vidfile
            print('Recording %s' % self.__recording.name)
            self.__state = RecState.RECORDING

    def pause(self):
        """pause if camera is recording, otherwise continue whatever camera was doing"""
        if self.__state == RecState.RECORDING:
            print('Paused %s' % self.__recording.name)
            self.camera.stop_recording()
            self.__state = RecState.PAUSED

    def resume(self):
        """resume if camera is paused, otherwise continue whatever camera was doing"""
        if self.__state == RecState.PAUSED:
            self.camera.start_recording(self.__recording)
            print('Resumed %s' % self.__recording.name)
            self.__state = RecState.RECORDING

    def stop(self):
        """stop if camera is recording or paused, otherwise continue whatever camera was doing
           return path to recorded file"""
        if self.__state == RecState.RECORDING or self.__state == RecState.PAUSED:
            self.camera.stop_recording()
            print('Stopped %s' % self.__recording.name)
            filename = self.__recording.name

            self.__recording.close()
            self.__recording = None
            self.__state = RecState.IDLE

            return filename

    def get_preview(self):
        """take a snapshot and return path to the snapshot"""
        self.camera.capture('data/preview.jpg', use_video_port=True)
        return 'data/preview.jpg'
