#!/usr/bin/env python3
from flask import Flask, request, send_file
from uploader import Uploader
from recorder import Recorder

app = Flask(__name__)

u = Uploader()
r = Recorder()

videofile = None

@app.route('/', methods=['GET', 'POST'])
def request_handler():
    if request.method == 'POST':
        if 'start' in request.form:
            r.start(request.form['start'])
            return 'started'
        elif 'pause' in request.form:
            r.pause()
            return 'paused'
        elif 'resume' in request.form:
            r.resume()
            return 'resumed'
        elif 'stop' in request.form:
            path = r.stop()
            if 'upload' in request.form:
                u.enqueue(path)
                return 'stopped, uploading'
            return 'stopped'
    else: # request.method == GET
        return 'Camera Preview: </br> <a href="/preview"><img src="/preview"></a>'

@app.route('/preview')
def preview():
    return send_file(r.get_preview())

if __name__ == '__main__':
    # temporary setup, runs simple web server on the default port: 5000
    app.run(debug=False, host='0.0.0.0')
