#!/usr/bin/env python

from queue import Queue
from threading import Thread
from os import path, remove
from youtube_upload import initialize_upload, get_authenticated_service

class YoutubeOpts():
    def __init__(self, file,
                 # String (defaults to filename):
                 title = None,
                 # String
                 description = "Test video for PCWCamera",
                 # None or String of comma separated tags
                 keywords = None,
                 # None or list of String
                 tags = None,
                 # check youtube_categories.json (1 is film and animation)
                 category = 1,
                 # String (public, private, unlisted)
                 privacyStatus = 'unlisted'):
        self.file = file
        if title:
            self.title = title
        else:
            # default to filename as title
            self.title = path.splitext(path.basename(file))[0]
        self.description = description
        self.keywords = keywords
        self.tags = tags
        self.category = category
        self.privacyStatus = privacyStatus

class Uploader():
    """maintains a queue of videos to upload and several worker threads to handle the uploads"""
    def __init__(self, num_workers=5):
        self.__upload_queue = Queue()
        youtube = get_authenticated_service("noauth_local_webserver")

        def worker():
            while True:
                # worker blocks until it retrieves something from queue
                vid_path = self.__upload_queue.get()
                if vid_path is None:
                    # a None object in the queue is the worker's signal to terminate
                    break
                #upload video with filepath form queue
                initialize_upload(youtube, YoutubeOpts(vid_path))
                print("%s finished upload" % vid_path)
                remove(vid_path)

                self.__upload_queue.task_done()

        self.__workers = [Thread(target=worker).start() for _ in range(num_workers)]

    def enqueue(self, vid_path):
        self.__upload_queue.put(vid_path)

    def stop_workers(self):
        # wait for all queued tasks to finish
        self.__upload_queue.join()
        # tell all workers to stop
        for _ in range(len(self.__workers)):
            self.__upload_queue.put(None)
        # wait for workers to stop
        for thread in self.__workers:
            thread.join()
